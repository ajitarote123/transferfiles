"use strict";

jQuery(document).ready(function ($) {
  /************** Menu Content Opening *********************/
  $(".main_menu a, .responsive_menu a").click(function () {
    var id = $(this).attr("class");
    id = id.split("-");
    $("#menu-container .content").hide();
    $("#menu-container #menu-" + id[1])
      .addClass("animated fadeInDown")
      .show();
    $("#menu-container .homepage").hide();
    $(".support").hide();
    $(".testimonials").hide();
    return false;
  });

  $(window).load(function () {
    $("#menu-container .products").hide();
  });

  $(".main_menu a.templatemo_home").addClass("active");

  $(".main_menu a.templatemo_home, .responsive_menu a.templatemo_home").click(
    function () {
      $("#menu-container .homepage").addClass("animated fadeInDown").show();
      $(this).addClass("active");
      $(".page-heading").html("About Us");
      $(
        ".main_menu a.templatemo_page2, .responsive_menu a.templatemo_page2"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_page4, .responsive_menu a.templatemo_page4"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_page3, .responsive_menu a.templatemo_page3"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_page5, .responsive_menu a.templatemo_page5"
      ).removeClass("active");
      return false;
    }
  );

  $(".main_menu a.templatemo_page2, .responsive_menu a.templatemo_page2").click(
    function () {
      $("#menu-container .team").addClass("animated fadeInDown").show();
      $(this).addClass("active");
      $(".page-heading").html("");
      $(
        ".main_menu a.templatemo_home, .responsive_menu a.templatemo_home"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_page4, .responsive_menu a.templatemo_page4"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_page3, .responsive_menu a.templatemo_page3"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_page5, .responsive_menu a.templatemo_page5"
      ).removeClass("active");
      return false;
    }
  );

  $(".main_menu a.templatemo_page3, .responsive_menu a.templatemo_page3").click(
    function () {
      $("#menu-container .services").addClass("animated fadeInDown").show();
      $(".our-services").show();
      $(this).addClass("active");
      $(".page-heading").html("");
      $(
        ".main_menu a.templatemo_page2, .responsive_menu a.templatemo_page2"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_page4, .responsive_menu a.templatemo_page4"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_home, .responsive_menu a.templatemo_home"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_page5, .responsive_menu a.templatemo_page5"
      ).removeClass("active");
      return false;
    }
  );

  $(".main_menu a.templatemo_page5, .responsive_menu a.templatemo_page5").click(
    function () {
      $("#menu-container .contact").addClass("animated fadeInDown").show();
      $(this).addClass("active");
      $(".page-heading").html("");
      $(
        ".main_menu a.templatemo_page2, .responsive_menu a.templatemo_page2"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_page4, .responsive_menu a.templatemo_page4"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_page3, .responsive_menu a.templatemo_page3"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_home, .responsive_menu a.templatemo_home"
      ).removeClass("active");

      loadScript();
      return false;
    }
  );

  $(".main_menu a.templatemo_page4, .responsive_menu a.templatemo_page4").click(
    function () {
      $("#menu-container .gallery").addClass("animated fadeInDown").show();
      $(this).addClass("active");
      $(".page-heading").html("");
      $(
        ".main_menu a.templatemo_page2, .responsive_menu a.templatemo_page2"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_page3, .responsive_menu a.templatemo_page3"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_page5, .responsive_menu a.templatemo_page5"
      ).removeClass("active");
      $(
        ".main_menu a.templatemo_home, .responsive_menu a.templatemo_home"
      ).removeClass("active");

      loadScript();
      return false;
    }
  );

  /************** Gallery Hover Effect *********************/
  $(".overlay").hide();

  $(".gallery-item").hover(
    function () {
      $(this).find(".overlay").addClass("animated fadeIn").show();
    },
    function () {
      $(this).find(".overlay").removeClass("animated fadeIn").hide();
    }
  );

  /************** LightBox *********************/
  $(function () {
    $('[data-rel="lightbox"]').lightbox();
  });

  $("a.menu-toggle-btn").click(function () {
    $(".responsive_menu").stop(true, true).slideToggle();
    return false;
  });

  $(".responsive_menu a").click(function () {
    $(".responsive_menu").hide();
  });

  var occasionList = [
    {
      content:
        "This year we celebrated Ganesh Puja in Kemfort Ground, Murgesh palya, Bengaluru in the presence of more than 10,000 devotees. Distributed FREE Prasada Sevan for more than 5000 people. In evening cultural program, our attraction was Babushan, Bhumika, Sabyasachi and Archita.",
    },
    {
      content:
        "Holi is a Hindu festival. It welcomes spring season an celebrated in the end of winter. OPC celebrated Holi near Jagannath temple, Agra with a huge crowd aroud 1000 people from different corner of Bangalore. OPC is the first organizaion in Bangalore, who started this culture and still continuing.",
    },
    {
      content:
        "OPC always extends helping hand to everyone without distigusing cast and culture. Once in every quarter, members of OPC visiting orphanage whenever they want to celebrate special occasion like Saraswati Puja, Inepenence Day, Repulic Day, Children's Day, Teachers' Day. Eid, Christmas and new year",
    },
    {
      content:
        "In this Pandemic, OPC did't step back. As always, without any fear in open mind and broad heart started helping to the needy people are present in different corner of India distributed relief package in Karnataka. And arranged communication services from Karnataka to Odisha, Tamilnadu to Odisha and Andhra Pradesh to odisha, keeping in mind that each and every people will reach safely to their destinations without any problems",
    },
    {
      content:
        "From 1994, OPC arranging ratha Yatra in Bengaluru with all rituals of Lord Jagannath. This Year with a crowd of 10,000 devotees celebrated ratha Yatra.",
    },
  ];

  // Achivements content section
  var achivementContainer = $(".achievement-occasion p");
  var seeMoreBtns = $(".achievement-occasion .see-more");
  occasionList.forEach(function (occ, i) {
    achivementContainer[i].innerHTML = occasionList[i].content.substr(0, 100)+'...';
    $(seeMoreBtns[i]).attr("state", "less");
    $(seeMoreBtns[i]).attr("data-index", i);
    $(seeMoreBtns[i]).text("see more");
  });
  $(".see-more").click(function () {
    var i = $(this).attr("data-index");
    var state = $(this).attr("state");
    if (state == "less") {
      achivementContainer[i].innerHTML = occasionList[i].content;
      $(seeMoreBtns[i]).attr("state", "more");
      $(seeMoreBtns[i]).text("see less");
    } else {
      achivementContainer[i].innerHTML = occasionList[i].content.substr(0, 100);
      $(seeMoreBtns[i]).attr("state", "less");
      $(seeMoreBtns[i]).text("see more");
    }
  });
});

function loadScript() {
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src =
    "https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&" +
    "callback=initialize";
  document.body.appendChild(script);
}

function initialize() {
  var mapOptions = {
    zoom: 12,
    center: new google.maps.LatLng(40.7823234, -73.9654161),
  };
  var map = new google.maps.Map(
    document.getElementById("templatemo_map"),
    mapOptions
  );
}
